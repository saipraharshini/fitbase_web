package stepDef;

import java.util.HashMap;





import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.hc.core5.util.Asserts;
import org.openqa.selenium.WebDriver;



import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import factory.DriverFactory;
import io.cucumber.datatable.DataTable;
import junit.framework.Assert;
import locators.LoginPageLocators; 
import util.ConfigReader;



public class LoginPage {
	
	private ConfigReader configreader=new ConfigReader();
	private Properties config2=new Properties();
	public Scenario scenario;

	
	
	
	
	private LoginPageLocators login = new LoginPageLocators(DriverFactory.getDriver());
	
	@Given("launch fitbase application")
	public void launch_fitbase_application() throws InterruptedException {
	    WebDriver driver=DriverFactory.getDriver();
		driver.get(configreader.init_prop().getProperty("url"));
		Asserts.check(true,"Fitbase - Wellness. Anywhere. Anytime.");
		String x=driver.getTitle();
		System.out.println("Page Title:" +x);
		Thread.sleep(3000);
	}


	@When("user clicks on SignUpLogIn button")
	public void user_clicks_on_sign_up_log_in_button() throws InterruptedException {
	    login.signUpLogIn.click();
	    Thread.sleep(2000);
	    login.loginText.isDisplayed();
	//	String logintext=login.loginText.getText();
	//	System.out.println("Login Text" +logintext);
	}
	@Then("user is on login page")
	public void user_is_on_login_page() {
		    String title=login.pageTitle.getText();
	   	 System.out.println("Page Title:" +title);
		
	}
	@When("user enters valid username and password")
	public void user_enters_valid_username_and_password(DataTable dataTable) throws Throwable {
//		List<Map<String, String>> credentials=dataTable.asMaps(String.class,String.class);
//		for (Map<String, String> data : dataTable.asMaps(String.class, String.class)) {  
//	    for(Map<String,String> e: credentials) {
		//	  login.Email(e.get("Username"));
		//	  login.Password(e.get("Password")); 
			  

			  
	/*    List<List<String>> rows=dataTable.asLists(String.class);
		for(List<String> columns: rows) {
			login.Email(columns.get(0));
			login.Password(columns.get(1)); */
		
	/*	String str=(e.get("Uername"));
		String str1=(e.get("Password"));
		int a=Integer.parseInt(str);
		int b=Integer.parseInt(str1); */
		
		
		
		List<List<String>> data = dataTable.cells();
		System.out.println(data.get(0).get(0));
		System.out.println(data.get(0).get(1));
		
		login.Email(data.get(0).get(0));
		Thread.sleep(2000);
		login.Password(data.get(0).get(1));
		}
			
	
	@When("click on Login")
	public void click_on_login() throws InterruptedException {
	    login.Login.click();
    	Thread.sleep(3000);
	}

	@When("Error Message displayed")
	public void Error_Message_displayed(DataTable table) throws InterruptedException {
		List<List<String>> data = table.cells();
		
		login.ErrMsg.isDisplayed();
		String Msg=login.ErrMsg.getText();
		
		System.out.println(Msg);
		
		if(login.ErrMsg.isDisplayed()) {
			System.out.println(data.get(0).get(0));
			login.Email(data.get(0).get(0));
			System.out.println(data.get(0).get(1));
			login.Password(data.get(0).get(1));
			login.Login.click();
			Thread.sleep(2000);
			login.Login.click();
			
			//DriverFactory.getDriver().navigate().refresh();
			Thread.sleep(3000);
			}
		 if(login.ErrMsg.isDisplayed()){
			System.out.println(data.get(1).get(0));
			login.Email(data.get(1).get(0));
			System.out.println(data.get(1).get(1));
			login.Password(data.get(1).get(1));
			login.Login.click();
			Thread.sleep(2000);
			DriverFactory.getDriver().navigate().refresh();
			Thread.sleep(3000);
		} 
		if(login.ErrMsg.isDisplayed()){
			System.out.println(data.get(2).get(0));
			login.Email(data.get(2).get(0));
			System.out.println(data.get(2).get(1));
			login.Password(data.get(2).get(1));
			login.Login.click();
			Thread.sleep(2000);
		}	
		else {
			System.out.println("Login button is not available");
		}
			WebDriver driver=DriverFactory.getDriver();
			String Title=driver.getTitle();
			System.out.println("User is on " +Title);
	}
}



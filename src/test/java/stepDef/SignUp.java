package stepDef;

import java.util.List;
import java.util.Properties;

import org.apache.hc.core5.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import factory.DriverFactory;
import io.cucumber.datatable.DataTable;

import locators.SignUpLocators;
import util.ConfigReader;

public class SignUp {
	
	
	private ConfigReader configreader=new ConfigReader();
	private Properties config2=new Properties();
	private SignUpLocators signup=new SignUpLocators(DriverFactory.getDriver());
	
	@Given("Launch Fitbase")
	public void Launch_Fitbase() throws InterruptedException {
		
		WebDriver driver=DriverFactory.getDriver();
		driver.get(configreader.init_prop().getProperty("url"));
		Asserts.check(true, "Fitbase - Wellness. Anywhere. Anytime.");
		String title=driver.getTitle();
		System.out.println("User is on " +title);
		Thread.sleep(2000);
	}
	
	@Then("user is redirected to Login page")
	public void user_is_redirected_to_Login_page() throws InterruptedException {
		signup.SignUpLogin.click();
		Thread.sleep(2000);
	}
	
	@When("user clicks on SignUp")
	public void user_clicks_on_SignUp() {
		signup.SignUp.click();
	}
	
	@When("user enters First Name")
	public void user_enters_First_Name(DataTable table) throws InterruptedException {
	WebDriver driver=DriverFactory.getDriver();
		List<List<String>> data=table.cells();
		System.out.println(data.get(0).get(0));
		signup.FirstName(data.get(0).get(0));
		
		Actions a=new Actions(driver);
        a.moveToElement(signup.CheckBox).build().perform();
        Thread.sleep(5000);
        a.click(signup.CheckBox).build().perform();
        Thread.sleep(2000);
        signup.Continue.click();
		String title=driver.getTitle();
	    System.out.println(title);
		if(title.equalsIgnoreCase("Signup")) {
			
			signup.FirstName.clear();
			signup.FirstName(data.get(1).get(0));
			System.out.println(data.get(1).get(0));
	        a.moveToElement(signup.CheckBox).build().perform();
	        Thread.sleep(5000);
	        a.click(signup.CheckBox).build().perform();
	        Thread.sleep(2000);
	        signup.Continue.click();
		}
		
		signup.SignUpText.isDisplayed();
		String text=signup.SignUpText.getText();
		System.out.println(text);
		if(text.equalsIgnoreCase("SIGN UP")) {
			
			signup.FirstNameErr.isDisplayed();
			String LenErr=signup.FirstNameErr.getText();
			System.out.println(LenErr);
			signup.FirstName.clear();
			signup.FirstName(data.get(2).get(0));
			System.out.println(data.get(2).get(0));
	        a.moveToElement(signup.CheckBox).build().perform();
	        Thread.sleep(5000);
	        a.click(signup.CheckBox).build().perform();
	        Thread.sleep(2000);
	        signup.Continue.click();
		} 
		else {
				System.out.println("FirstName accepted");
		}  
	}
	@Then("enters Last Name")
	public void enters_Last_Name(DataTable table) throws InterruptedException {
		WebDriver driver=DriverFactory.getDriver();
		List<List<String>> data=table.cells();
		System.out.println(data.get(0).get(0));
		signup.LastName(data.get(0).get(0));
		Actions a=new Actions(driver);
		a.moveToElement(signup.CheckBox).build().perform();
        Thread.sleep(5000);
        a.click(signup.CheckBox).build().perform();
        Thread.sleep(2000);
        signup.Continue.click();
      String title=driver.getTitle();
        System.out.println(title);
        if(title.equalsIgnoreCase("Signup")) {
        	signup.LastName.clear();
        	signup.LastNameErr.isDisplayed();
        	String errmsg=signup.LastNameErr.getText();
        	System.out.println(errmsg);
        	signup.LastName(data.get(1).get(0));
        	System.out.println(data.get(1).get(0));
        	a.moveToElement(signup.CheckBox).build().perform();
            Thread.sleep(5000);
            a.click(signup.CheckBox).build().perform();
            Thread.sleep(2000);
            signup.Continue.click();
        }
        else {
        	System.out.println("LastName accepted");
        }  
	}
	@Then("enters Email") 
	public void enters_Email(DataTable table) throws InterruptedException {
		WebDriver driver=DriverFactory.getDriver();
		List<List<String>> data=table.cells();
		System.out.println(data.get(0).get(0));
		signup.Email(data.get(0).get(0));
		Actions a=new Actions(driver);
		a.moveToElement(signup.CheckBox).build().perform();
        Thread.sleep(5000);
        a.click(signup.CheckBox).build().perform();
        Thread.sleep(2000);
        signup.Continue.click();
        String title=driver.getTitle();
        System.out.println(title);
        if(title.equalsIgnoreCase("Signup")) {
        	signup.EmailErrMsg.isDisplayed();
        	String err=signup.EmailErrMsg.getText();
        	System.out.println(err);
        	signup.Email.clear();
        	System.out.println(data.get(1).get(0));
        	signup.Email(data.get(1).get(0));
        	a.moveToElement(signup.CheckBox).build().perform();
            Thread.sleep(5000);
            a.click(signup.CheckBox).build().perform();
            Thread.sleep(2000);
            signup.Continue.click();
         }
        else {
        	System.out.println("Email accepted");
        }  
	}
	@Then("enters password")
	public void enters_password(DataTable table) throws InterruptedException {
		WebDriver driver=DriverFactory.getDriver();
		List<List<String>> data=table.cells();
		Actions a=new Actions(driver);
		System.out.println(data.get(0).get(0));
		signup.password(data.get(0).get(0));		
		a.moveToElement(signup.CheckBox).build().perform();
        Thread.sleep(5000);
        a.click(signup.CheckBox).build().perform();
        Thread.sleep(2000);
        signup.Continue.click();
        String pageTitle=driver.getTitle();
		System.out.println(pageTitle);
        Thread.sleep(3000);
		if(pageTitle.equalsIgnoreCase("Signup")) {
			signup.PwdErrMsg.isDisplayed();
			String errmsg=signup.PwdErrMsg.getText();
			System.out.println(errmsg);
			signup.password.clear();
			System.out.println(data.get(1).get(0));
			signup.password(data.get(1).get(0));
			a.moveToElement(signup.CheckBox).build().perform();
	        Thread.sleep(5000);
	        a.click(signup.CheckBox).build().perform();
	        Thread.sleep(2000);
	        signup.Continue.click(); 
		} else {
			System.out.println("Password accepted");
		}
		signup.SignUpText.isDisplayed();
		String text=signup.SignUpText.getText();
		System.out.println(text);
		if(text.equalsIgnoreCase("SIGN UP")) {
			signup.EmailExistsErr.isDisplayed();
			String errmsg1=signup.EmailExistsErr.getText();
			System.out.println(errmsg1);
			signup.Email.clear();
			System.out.println(data.get(2).get(0));
			signup.Email(data.get(2).get(0));
			signup.password.clear();
			System.out.println(data.get(1).get(0));
			signup.password(data.get(1).get(0));
			
		}
	}
	
	@Then("user clicks on continue")
	public void user_clicks_on_continue() throws InterruptedException{
		WebDriver driver=DriverFactory.getDriver();
		Actions a=new Actions(driver);
        a.moveToElement(signup.CheckBox).build().perform();
        Thread.sleep(5000);
        a.click(signup.CheckBox).build().perform();
        Thread.sleep(2000);
        signup.Continue.click(); 
        String title=driver.getTitle();
        System.out.println(title);
        if(title.equalsIgnoreCase("Signup")) {
        	signup.FirstNameErr.isDisplayed();
        	String fnerr=signup.FirstNameErr.getText();
        	System.out.println(fnerr);
        	signup.LastNameErr.isDisplayed();
        	String lnerr=signup.LastNameErr.getText();
        	System.out.println(lnerr); 
        	signup.EmailErrMsg.isDisplayed();
        	String err2=signup.EmailErrMsg.getText();
        	System.out.println(err2);
        	signup.PwdErrMsg.isDisplayed();
        	String pwderr=signup.PwdErrMsg.getText();
        	System.out.println(pwderr);
        }
        else {
        	System.out.println("SignUp successfull. User is on " +title);
        }
	}
	
	@Then("user lands on Health Profile page")
	public void user_lands_on_Health_Profile_Page() {
		WebDriver driver=DriverFactory.getDriver();
        String title=driver.getTitle();
        System.out.println("SignUp Succesfull. User is on "+title);
        signup.Skip.click();
		signup.NotNow.click();
		String title1=driver.getTitle();
        System.out.println("User is on "+title1);  
		
	} 
}

package stepDef;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.hc.core5.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import factory.DriverFactory;
import io.cucumber.datatable.DataTable;


import locators.ForgotPassword;
import util.ConfigReader;

public class ForgotPasswordMain {
	
	private ConfigReader configreader=new ConfigReader();
	private Properties config2=new Properties();
	
	private ForgotPassword FPwd=new ForgotPassword(DriverFactory.getDriver());
	
	@Given("Launch Fitbase Application") 
	public void Launch_Fitbase_Application() throws InterruptedException {
		WebDriver driver=DriverFactory.getDriver();
		driver.get(configreader.init_prop().getProperty("url"));
		driver.get("https://qa.fitbase.com/");
		Asserts.check(true,"Fitbase - Wellness. Anywhere. Anytime.");
		String x=driver.getTitle();
		System.out.println("Page Title:" +x);
		Thread.sleep(3000);
	}
	
	@Then("User is on login page")
	public void User_is_on_login_page() throws InterruptedException {
		FPwd.SignUpLogIn.click();
	    Thread.sleep(2000);
	}
	
	
	@When("user clicks on forgot password")
	public void user_clicks_on_forgot_password() {
		FPwd.ForgotPwd.click();
	}
	
	@And("Enters valid Email") 
	public void Enters_valid_Email(DataTable table) throws InterruptedException {
		List<List<String>> data= table.cells();
		String email = (data.get(0).get(0));
		System.out.println(email);
		Thread.sleep(2000);
		FPwd.EmailReset(data.get(0).get(0));
	}
	
	@Then("click on send reset link") 
	public void click_on_send_reset_link() throws InterruptedException {
		FPwd.ResetLinkButton.click();
		Thread.sleep(3000);
		
	}
	
	@When("user gets error msg")
	public void user_gets_error_msg(DataTable table) {
		List<List<String>> data=table.cells();
		String url =DriverFactory.getDriver().getCurrentUrl();
		System.out.println(url);
		String pagetitle=DriverFactory.getDriver().getTitle();
		System.out.println(pagetitle);
		if(pagetitle.equalsIgnoreCase("Forgot Password")) 
		{
			FPwd.InvalidErrMsg.isDisplayed();
			String ErrMsg=FPwd.InvalidErrMsg.getText();
			System.out.println(ErrMsg);
			FPwd.EmailReset.clear();
			FPwd.EmailReset(data.get(0).get(0));
			System.out.println(data.get(0).get(0));
			FPwd.ResetLinkButton.click();
			String ErrMsg1=FPwd.InvalidErrMsg.getText();
			System.out.println(ErrMsg1);
		//	 if(ErrMsg1.equalsIgnoreCase("There is no such email address."))	{
			 if(ErrMsg1==ErrMsg1)	{
			/*	FPwd.NoSuchErrMsg.isDisplayed();
				String NoSuchMsg=FPwd.NoSuchErrMsg.getText();
				System.out.println(NoSuchMsg);  */
				FPwd.EmailReset.clear();
				FPwd.EmailReset(data.get(1).get(0));
				System.out.println(data.get(1).get(0));
				FPwd.ResetLinkButton.click();
			}
			 else {
				 System.out.println(ErrMsg);
			 }
		}
		else
		{
			FPwd.EmailSentMsg.isDisplayed();
			String Msg=FPwd.EmailSentMsg.getText();
			System.out.println(Msg);
			
		}  
	}
	@And("now go to mailinator site and open mail")
	public void now_go_to_mailinator_site_and_open_mail(DataTable table) throws InterruptedException {
		
		WebDriver driver=DriverFactory.getDriver();
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tab=new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tab.get(1));
		driver.get("https://www.mailinator.com/v4/public/inboxes.jsp");
		List<List<String>> data=table.cells();
		FPwd.MailBoxEnter(data.get(0).get(0));
		System.out.println(data.get(0).get(0));
		FPwd.GoClick.click();
		WebDriverWait wait=new WebDriverWait(driver,30);
		Thread.sleep(2000);
		FPwd.EmailOpen.click();
		Thread.sleep(2000);
		WebElement iframe = driver.findElement(By.tagName("iframe"));
		driver.switchTo().frame(iframe);  		
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(FPwd.ChangePwd));
		FPwd.ChangePwd.click();
		ArrayList<String> tab2=new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tab2.get(2));
	}
	
	@Then("user resets the password") 
	public void user_resets_the_password(DataTable table) throws InterruptedException {
		List<List<String>> data=table.cells();
		System.out.println(data.get(0).get(0));
		System.out.println(data.get(0).get(1));
		FPwd.Password(data.get(0).get(0));
		FPwd.ConPassword(data.get(0).get(1));
		FPwd.SaveButton.click();
		if(FPwd.SaveButton.isDisplayed()) {
			FPwd.ResetPwdErr.isDisplayed();
			String ErrMsg=FPwd.ResetPwdErr.getText();
			System.out.println(ErrMsg);
			System.out.println(data.get(1).get(0));
			System.out.println(data.get(1).get(1));
			FPwd.Password(data.get(1).get(0));
			FPwd.ConPassword(data.get(1).get(1));
			FPwd.SaveButton.click();
			FPwd.SuccessMsg.isDisplayed();
			String Msg=FPwd.SuccessMsg.getText();
			System.out.println(Msg);
		}
		else
		{
			FPwd.SuccessMsg.isDisplayed();
			String Msg=FPwd.SuccessMsg.getText();
			System.out.println(Msg);
		}
		
		WebDriver driver=DriverFactory.getDriver();
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(FPwd.LoginLink));
		FPwd.LoginLink.click();
		Thread.sleep(3000);
		
	}
	
	@When("user login with valid credentials")
	public void user_login_with_valid_credentails(DataTable table) throws InterruptedException {
		List<List<String>> data=table.cells();
		FPwd.SignUpLogIn.click();
	    Thread.sleep(2000);
		System.out.println(data.get(0).get(0));
		System.out.println(data.get(0).get(1));
		FPwd.Email(data.get(0).get(0));
		FPwd.Pwd(data.get(0).get(1));
		FPwd.Login.click();
	}
	
	@Then("User lands on Dashboard page")
	public void User_lands_on_Dashboard_page() {
		WebDriver driver=DriverFactory.getDriver();
		String title=driver.getTitle();
		
		if(title.equalsIgnoreCase("Dashboard")) {
			
			System.out.println("User is on " +title);
		}
		else {
				System.out.println("Error while login " +title);
		}
		
	}

}

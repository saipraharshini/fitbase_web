package hooks;

//import java.io.File;
import java.util.Properties;

//import org.junit.AfterClass;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

//import com.cucumber.listener.Reporter;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import factory.DriverFactory;
//import managers.FileReaderManager;
import util.ConfigReader;

public class ApplicationHooks {
	private WebDriver driver;
	private DriverFactory driverFactory;
	private ConfigReader configReader;
	Properties prop;
	
@Before(order = 0)
public void getProperty(){
	configReader = new ConfigReader();
	prop = configReader.init_prop();	
}
@Before(order = 1)
public void lanuchBrowser(){
	String browserName = prop.getProperty("browser");
	driverFactory = new DriverFactory();
	driver = driverFactory.init_driver(browserName);
}	

@After(order = 1)
public void tearDown(Scenario scenario){
	if(scenario.isFailed()){
		//take Screenshot
		String screenshotName = scenario.getName().replaceAll(" ", "_");
		byte[] sourcePath = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
	//	scenario.a.attach(sourcePath,"image/png", screenshotName );
	}
}
@After(order = 0)
public void quitBrowser(){
	//driver.quit();
}
/* @After(order = 2)
public void writeExtentReport() {
	Reporter.loadXMLConfig(new File(FileReaderManager.getInstance().getConfigReader().getReportConfigPath()));
} */
}

package testRunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
features = "src/test/resources/Features/Login.feature",
glue = {"stepDef" ,"hooks" },
plugin = { "pretty", "json:target/cucumber.json"},
dryRun = false,
monochrome = true)


public class testRunner {

}
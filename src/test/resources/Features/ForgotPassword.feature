Feature: Forgot Password for Fitbase Web

Scenario: Validating Forgot Password 

	Given Launch Fitbase Application
	Then User is on login page 
	When user clicks on forgot password 
	And Enters valid Email
			| praha12@mailinator.com |
	Then click on send reset link 
	When user gets error msg
			| praha12@mailinator |
			| sunilkumar15@mailinator.com |
	And now go to mailinator site and open mail
		| sunilkumar15 |
	Then user resets the password
		| password | conpassword |
		| password | password    |
	When user login with valid credentials
		| sunilkumar15@mailinator.com | password |
	Then User lands on Dashboard page
	
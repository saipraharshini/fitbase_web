package locators;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageLocators {
	
	
	@FindBy(xpath="(//*[@id='hidemenu1']/li[8]/a)")
	public WebElement signUpLogIn; 
	
	@FindBy(xpath="(//*[@class='customheader'])")
	public WebElement pageTitle;
	
	@FindBy(xpath="(//*[@id='username'])")
	public WebElement Email;
	
	@FindBy(xpath="(//*[@id='password'])")
	public WebElement Password;

	@FindBy(xpath="(//*[@id='loginemail'])")
	public WebElement Login;
		
	@FindBy(xpath=("//*[@class='has-error']/span"))
	public WebElement ErrMsg;
	
	@FindBy(xpath=("//*[@id='loginemail']/span"))
    public WebElement loginText;
	
	public LoginPageLocators(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void signUpLogIn() {
		signUpLogIn.click();
		
	}
	public void pageTitle() {
		pageTitle.isDisplayed();
	}
	
	public void Email(String username) {
		Email.sendKeys(username);
	}
	
	public void Password(String password) {
		Password.sendKeys(password);
	}
	
	public void Login(){
		Login.click();
	}
	
	public void ErrMsg() {
		ErrMsg.isDisplayed();
	}
    public void loginText() {
    	loginText.isDisplayed();
    }
	
}

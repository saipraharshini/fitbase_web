package locators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ForgotPassword {

	@FindBy(xpath="(//*[@id='hidemenu1']/li[8]/a)")
	public WebElement SignUpLogIn; 
	
	@FindBy(xpath=("//*[@class='pull-right']/a"))
	public WebElement ForgotPwd;
	
	@FindBy(xpath=("//*[@id='email']"))
	public WebElement EmailReset;
	
	@FindBy(xpath=("//*[@type='submit']/span"))
	public WebElement ResetLinkButton;
	
	@FindBy(xpath=("//*[@class='col-xs-12 btm-holder mt-box lit-col']/h4"))
	public WebElement EmailSentMsg;
	
	@FindBy(linkText=("Back to Log In"))
	public WebElement BackButton;

	@FindBy(xpath=("//*[@class='form-group has-error']/span"))
	public WebElement InvalidErrMsg;
				
//	@FindBy(xpath=("//*[@class='form-group has-error']/span"))
//	public WebElement  NoSuchErrMsg;
	
	@FindBy(xpath=("//*[@id='inbox_field']"))
	public WebElement MailBoxEnter;
	
	@FindBy(xpath=("//*[@class='primary-btn']"))
	public WebElement GoClick;
	
	@FindBy(xpath=("(//*[@class='os-content'])[5]/child::*/tbody/tr"))
	public WebElement EmailOpen;
	
	@FindBy(tagName=("iframe"))
	public WebElement iframe;
	
	@FindBy(xpath=("((//*[@width='auto'])[2]/tbody/tr)[2]/td/a"))
	public WebElement ChangePwd; 
	
	@FindBy(id=("password"))
	public WebElement password;
	
	@FindBy(id=("conpassword"))
	public WebElement conpassword;
	
	@FindBy(xpath=("//button[@class='btn btn-primary btn-lg btn-block']"))
	public WebElement SaveButton;
	
	@FindBy(xpath=("(//*[@class='help-block'])[1]"))
	public WebElement ResetPwdErr;
	
	@FindBy(xpath=("//*[@class='col-xs-12 btm-holder mt-box lit-col']/h4"))
	public WebElement SuccessMsg;
	
	@FindBy(xpath=("//*[@class='col-xs-12 btm-holder mt-box lit-col']/h4/a"))
	public WebElement LoginLink;
	
	@FindBy(xpath="(//*[@id='username'])")
	public WebElement Email;
	
	@FindBy(xpath="(//*[@id='password'])")
	public WebElement Pwd;

	@FindBy(xpath="(//*[@id='loginemail'])")
	public WebElement Login;
	
	@FindBy(xpath=("//*[@class='has-error']/span)"))
	public WebElement LoginErr;
		
	public ForgotPassword(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
		
	public void SignUpLoginIn() {
		SignUpLogIn.click();
	}
	
	public void ForgotPwd() {
		ForgotPwd.click();
	}
	
	public void EmailReset(String email) {
		EmailReset.sendKeys(email);
	}
	
	public void ResetLinkButton() {
		ResetLinkButton.click();
	}
	
	public void EmailSentMsg() {
		EmailSentMsg.isDisplayed();
	}
	
	public void BackButton() {
		BackButton.click();
	}
	
	public void InvalidErrMsg() {
		InvalidErrMsg.isDisplayed();
	}
	
//	public void NoSuchErrMsg() {
//		NoSuchErrMsg.isDisplayed();	}
	
	public void MailBoxEnter(String email) {
		MailBoxEnter.sendKeys(email);
	}
	
	public void GoClick() {
		GoClick.click();
	}
	
	public void EmailOpen() {
		EmailOpen.click();
	}
	
	public void iframe() {
		iframe.isDisplayed();
	}
	
	public void ChangePwd() {
		ChangePwd.click();
	}
	
	public void Password(String pwd) {
		password.sendKeys(pwd);
	}
	
	public void ConPassword(String conpwd) {
		conpassword.sendKeys(conpwd);
	}
	
	public void SaveButton() {
		SaveButton.click();
	}
	
	public void ResedPwdErr() {
		ResetPwdErr.isDisplayed();
	}
	
	public void SuccessMsg() {
		SuccessMsg.isDisplayed();
	}
	
	public void LoginLink() {
		LoginLink.click();
	}
	
	public void Email(String username) {
		Email.sendKeys(username);
	}
	
	public void Pwd(String password) {
		Pwd.sendKeys(password);
	}
	
	public void Login(){
		Login.click();
	}
	
	public void LoginErr() {
		LoginErr.isDisplayed();
	}
}



package locators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpLocators {
	
	@FindBy(xpath="(//*[@id='hidemenu1']/li[8]/a)")
	public WebElement SignUpLogin; 
	
	@FindBy(linkText=("Sign Up"))
	public WebElement SignUp;
	
	@FindBy(xpath=("//*[@class='customheader']"))
	public WebElement SignUpText;
	
	@FindBy(xpath=("//*[@id='firstName']"))
	public WebElement FirstName;
	
	@FindBy(xpath=("//*[@id='firstName']/following::*"))
	public WebElement FirstNameErr;
	
//	@FindBy(xpath=("//*[@class='help-block']"))
//	public WebElement FirstLenErr;
	
	@FindBy(xpath=("//*[@id='lastName']"))
	public WebElement LastName;
	
	@FindBy(xpath=("//*[@id='lastName']/following::*"))
	public WebElement LastNameErr;
	
//	@FindBy(xpath=("//*[@class='help-block']"))
//	public WebElement LastLenErr;
	
	@FindBy(xpath=("//*[@id='email']"))
	public WebElement Email;
	
	@FindBy(xpath=("(//*[@placeholder='Email']/following::span)[1]"))
	public WebElement EmailErrMsg;
	
	@FindBy(xpath=("//*[@class='form-group has-error']/span"))
	public WebElement EmailExistsErr;
	
	@FindBy(xpath=("//*[@id='password']"))
	public WebElement password;
	
	@FindBy(xpath=("//*[@id='password']/following::span"))
	public WebElement PwdErrMsg;
	
//	@FindBy(xpath=("//*[@class='help-block']"))
//	public WebElement PwdLenErr;
	
	@FindBy(xpath=("//*[@for='terms']/parent::*"))
	public WebElement CheckBox;
	
	@FindBy(xpath=("//*[@id='submitbtn']"))
	public WebElement Continue;
	
	@FindBy(xpath=("//*[@class='btn btn-lg btn-info rm ng-binding']"))
	public WebElement Skip;
	
	@FindBy(xpath=("(//*[@class='mzero ng-binding']/b/a)[1]"))
	public WebElement NotNow;
	
	public SignUpLocators(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
	
	public void SignUpLogin() {
		SignUpLogin.click();
	}
	public void SignUp() {
		SignUp.click(); 
	}
	
	public void SignUpText() {
		SignUpText.isDisplayed();
	}
	
	public void FirstName(String firstname) {
		FirstName.sendKeys(firstname);
	}
	
	public void FirstNameErr() {
		FirstNameErr.isDisplayed();
	}
	
	public void LastName(String lastname) {
		LastName.sendKeys(lastname);
	}
	
	public void LastNameErr() {
		LastNameErr.isDisplayed();
	}
	
	public void Email(String email) {
		Email.sendKeys(email);
	}
	
	public void EmailErrMsg() {
		EmailErrMsg.isDisplayed();
	}
	
	public void EmailExistsErr() {
		EmailExistsErr.isDisplayed();
	}
	
	public void password(String pwd) {
		password.sendKeys(pwd);
	}
	
	public void PwdErrMsg() {
		PwdErrMsg.isDisplayed();
	}
	
	public void CheckBox() {
		CheckBox.isDisplayed();
	}
	
	public void Continue() {
		Continue.click();
	}
	
	public void Skip() {
		Skip.click();
	}
	
	public void NotNow() {
		NotNow.click();
	}
}

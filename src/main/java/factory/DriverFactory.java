package factory;


import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import util.ConfigReader;


public class DriverFactory {
public WebDriver driver;
public ConfigReader configreader=new ConfigReader();

	/* ThreadLocal is used for parallel execution so that when two threads are executing the same code, the two threads cannot
	see each other's ThreadLocal variables */
	public static ThreadLocal<WebDriver> tldriver = new ThreadLocal<WebDriver>();
	
	/* Init method is a predefined method to initialize an object after its creation. Init method is a life cycle method for 
	 * servlets for java. It is started by the browser when java program is loaded and run by the browser*/
	
	public WebDriver init_driver(String browser) {
	                         //equalsIgnoreCase is used for to ignore low and upper case while invoking the browser
		if (browser.equalsIgnoreCase("chrome")){
			WebDriverManager.chromedriver().setup();
			tldriver.set(new ChromeDriver()); 
		}
		else if(browser.equalsIgnoreCase("firefox")){
			WebDriverManager.firefoxdriver().setup();
			tldriver.set(new FirefoxDriver());
		}
		else if(browser.equalsIgnoreCase("Edge")) {
			WebDriverManager.edgedriver().setup();
			tldriver.set(new EdgeDriver());
		}
		else {
			System.out.println("Please enter the browser name:"+browser);
		}
		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
		return getDriver();
	}
		
	/* When a thread invokes a synchronized method, it automatically acquires the lock for that object and releases it when 
	 * the thread completes its task. */	
	public static synchronized WebDriver getDriver() {
		return tldriver.get();
	}
	
	}

	
package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

	public Properties config2;
	private Properties properties;
	
	/**
	 * 
	 * This method is used to load the properties from config,properties File
	 * @return Properties Object
	 */
	public Properties init_prop(){
	
		config2 = new Properties() ;
	
		try {
			FileInputStream input  = new FileInputStream("src\\test\\resources\\config\\config.properties");
		    config2.load(input); 
		} catch (FileNotFoundException e) {
			e.printStackTrace(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	return config2;
}
	
	public String getReportConfigPath(){
		String reportConfigPath = properties.getProperty("reportConfigPath");
		if(reportConfigPath!= null) return reportConfigPath;
		else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");		
	}

}
